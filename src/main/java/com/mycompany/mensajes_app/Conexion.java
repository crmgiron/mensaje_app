/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



/**
 *
 * @author crmgi
 */
public class Conexion {
    
   public Connection get_connection(){ //Metodo de conexion 
       Connection conection = null; //Objeto conexion
       try{ //Intentamos conectarnos a la base de datos
           conection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensaje_app","root", ""); // Parametros para conectarlos a la base de datos "url", "user", "password"
           if(conection != null){ //si pudo hacer la conexlion que nos mande un mensajito de conexion exitosa
               System.out.println("Conexion Exitosa (: ");
           }
       }catch(SQLException e) { // si no se pudo realizar la conexion que capture el SQLException
           System.out.println(e); // y nos los muestre en pantalla
       }
       return conection; // como es un metodo espera el retorno de nuestro objeto
   }
    
}
